/**
 * @file config_robotdyn_f303cc.h
 * @brief
 * @author Vladimir Postnov
 * @date 14/06/2022
 * @addtogroup
 * @{
*/

#ifndef CONFIG_ROBOTDYN_F303CC_H_
#define CONFIG_ROBOTDYN_F303CC_H_

#ifdef __cplusplus
extern "C" {
#endif

// ========================================= Definition ============================================

/**
 * @brief Модификации плат
 * @addtogroup DEVICE_HW__MODIFICATIONS
 * @{
 */
  /// @note Базовая модификация без изменений
  #define HW_MOD_0                                0
/** @} */

/// Выбранная модификация платы
#define HW_MODIFICATION                           HW_MOD_0

/// Смещение таблицы прерываний
#define VECT_TAB_OFFSET                           0x00000

/**
 * @defgroup LEDS
 * @brief Описание светодиодов
 * @{
 */
  #define LED_PORT_IDX                            2
  #define LED_PIN_IDX                             13
/** @} */

/**
 * @defgroup Управление работой Flash
 * @brief FLASH_SETTINGS
 * @{
 */
  /// Будет ли в проект добавлена работа с NVRAM на встроенной flash
  #define FEATURE_ENABLE_INT_NVRAM                1

  /// Будет ли в проект добавлена работа с NVRAM на врешней nor-flash семейства xx25
  #define FEATURE_ENABLE_EXT_NOR_XX25_NVRAM       0

  /// Будет ли в проект добавлена работа с NVRAM на врешней nor-flash семейства xx45
  #define FEATURE_ENABLE_EXT_NOR_XX45_NVRAM       0

  /// Размер NVRAM памяти.
  /// Не должен превышать размера одного семента внутренней flash
  /// Должен быть кратен 256 байтам
  #define NVRAM_SIZE                              256
/** @} */

#define FEATURE_ENABLE_UART1                      0
#define FEATURE_ENABLE_UART2                      0
#define FEATURE_ENABLE_UART3                      0
#define FEATURE_ENABLE_UART4                      0
#define FEATURE_ENABLE_UART5                      0
#define FEATURE_ENABLE_UART6                      0
#define FEATURE_ENABLE_UART7                      0
#define FEATURE_ENABLE_UART8                      0

#define FEATURE_ENABLE_ADC1                       0
#define FEATURE_ENABLE_ADC2                       0
#define FEATURE_ENABLE_ADC3                       0
#define FEATURE_ENABLE_ADC4                       0

#define FEATURE_ENABLE_DMA1                       1
#define FEATURE_ENABLE_DMA2                       0

#define FEATURE_ENABLE_SPI1                       0
#define FEATURE_ENABLE_SPI2                       0
#define FEATURE_ENABLE_SPI3                       0
#define FEATURE_ENABLE_SPI4                       0

#define FEATURE_ENABLE_I2S2                       0
#define FEATURE_ENABLE_I2S3                       0


// ========================================= Declaration ===========================================


#ifdef __cplusplus
}
#endif

#endif /* CONFIG_ROBOTDYN_F303CC_H_ */

/** @} */
