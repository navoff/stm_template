/**
 * @file config_nucleo_f411re.h
 * @author Vladimir Postnov
 * @date 23 февр. 2019 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef CONFIG_NUCLEO_F411RE_H_
#define CONFIG_NUCLEO_F411RE_H_

// ======================================= Definition ==============================================

/**
 * @brief Модификации плат
 * @addtogroup DEVICE_HW__MODIFICATIONS
 * @{
 */
  /// @note Базовая модификация без изменений
  #define HW_MOD_0                                0
/** @} */

/// Выбранная модификация платы
#define HW_MODIFICATION                           HW_MOD_0

/// Смещение таблицы прерываний
#define VECT_TAB_OFFSET                           0x0000

#define FEATURE_ENABLE_USB                        0

#define FEATURE_ENABLE_IWDG                       0

/**
 * @defgroup Управление работой Flash
 * @brief FLASH_SETTINGS
 * @{
 */
  /// Будет ли в проект добавлена работа с NVRAM на встроенной flash
  #define FEATURE_ENABLE_INT_NVRAM                1

  /// Будет ли в проект добавлена работа с NVRAM на врешней nor-flash семейства xx25
  #define FEATURE_ENABLE_EXT_NOR_XX25_NVRAM       0

  /// Будет ли в проект добавлена работа с NVRAM на врешней nor-flash семейства xx45
  #define FEATURE_ENABLE_EXT_NOR_XX45_NVRAM       0

  /// Размер NVRAM памяти.
  /// Не должен превышать размера одного семента внутренней flash
  /// Должен быть кратен 256 байтам
  #define NVRAM_SIZE                              2048
/** @} */

#if (FEATURE_ENABLE_IWDG)
  /// Время таймаута работы IWDG, мс
  #define IWDG_TIMEOUT                            3000

  /// Будет ли выполнено измерение частоты LSI с помощью TIM5
  #define IWDG_LSI_FREQ_MEASURE_EN                0
#endif

#if (FEATURE_ENABLE_USB)
  #define USE_USB_VCP_EN                          1
  #define VENDOR_ID                               0x0483
  #define PRODUCT_ID                              0x5740
#endif

/**
 * @defgroup LEDS
 * @brief Описание светодиодов
 * @{
 */
  #define LED_PORT_IDX                            0
  #define LED_PIN_IDX                             5
/** @} */


#define FEATURE_ENABLE_DMA1                       1
#define FEATURE_ENABLE_DMA2                       0

#define FEATURE_ENABLE_UART1                      0
#define FEATURE_ENABLE_UART2                      0
#define FEATURE_ENABLE_UART3                      0
#define FEATURE_ENABLE_UART4                      0
#define FEATURE_ENABLE_UART5                      0
#define FEATURE_ENABLE_UART6                      0
#define FEATURE_ENABLE_UART7                      0
#define FEATURE_ENABLE_UART8                      0

#define FEATURE_ENABLE_SPI1                       0
#define FEATURE_ENABLE_SPI2                       0
#define FEATURE_ENABLE_SPI3                       0
#define FEATURE_ENABLE_SPI4                       0

#define FEATURE_ENABLE_ADC1                       0
#define FEATURE_ENABLE_ADC2                       0
#define FEATURE_ENABLE_ADC3                       0
#define FEATURE_ENABLE_ADC4                       0

// ======================================= Declaration =============================================

#endif /* CONFIG_NUCLEO_F411RE_H_ */

/** @} */
