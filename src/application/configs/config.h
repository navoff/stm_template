/**
 * @file config.h
 * @brief
 * @author Vladimir Postnov
 * @date 27 янв. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef CONFIG_H_
#define CONFIG_H_

// ======================================= Definition ==============================================

#if defined(DEBUG)
  #define USE_CHECK_PARAMS_EN                 1
#elif defined(RELEASE)
  #define USE_CHECK_PARAMS_EN                 0
#else
  #error В настройках проекта нужно указать тип сборки для данной цели компиляции!
#endif

/**
 * @brief Версия прошивки
 * @addtogroup FW_VERSIONS
 * @{
 */
  /// Версия прошивки
  #define DEVICE_INFO__FW_VERSION_MAJOR       1

  /// Подверсия прошивки
  #define DEVICE_INFO__FW_VERSION_MINOR       0

  /// Версия сборки
  #define DEVICE_INFO__FW_BUILD               0xFFFF
/** @} */

/**
 * @defgroup HW_TYPE_LIST
 * Типы (идентификаторы) плат.
 * @{
 */
  /// Отладочная плата NUCLEO-F103RB
  #define HW_TYPE__NUCLEO_F103RB              1

  /// Отладочная плата STM32F3DISCOVERY
  #define HW_TYPE__STM32F3DISCOVERY           2

  /// Отладочная плата NUCLEO-F411RE
  #define HW_TYPE__NUCLEO_F411RE              3

  /// Плата от RobotDyn STM32 MINI с контроллером stm32f303cc
  #define HW_TYPE__ROBODYN_F303CC             4
/** @} */

/**
 * @defgroup FW_TYPE_LIST
 * Тип (идентификаторы) прошивок
 * @{
 */

  /// Загрузчик
  #define FW_TYPE__DEFAULT                    0x00000001

/** @} */

// Тип платы выбирать здесь:
#if !defined(HW_TYPE)
  //#define HW_TYPE                             HW_TYPE__NUCLEO_F103RB
  //#define HW_TYPE                             HW_TYPE__STM32F3DISCOVERY
  //#define HW_TYPE                             HW_TYPE__NUCLEO_F411RE
  //#define HW_TYPE                             HW_TYPE__ROBODYN_F303CC
#endif

// Тип прошивки выбирать здесь:
#define FW_TYPE                               FW_TYPE__DEFAULT



#if (HW_TYPE == HW_TYPE__NUCLEO_F103RB)

  #include "config_nucleo_f103rb.h"
  #include "stm32f10x.h"
  #include "stm32f10x_conf.h"
  #define STM32F1XX

#elif (HW_TYPE == HW_TYPE__STM32F3DISCOVERY)

  #include "config_stm32f3discovery.h"
  #include "stm32f30x.h"
  #include "stm32f30x_conf.h"
  #define STM32F30X

#elif (HW_TYPE == HW_TYPE__NUCLEO_F411RE)

  #include "config_nucleo_f411re.h"
  #include "stm32f4xx.h"
  #include "stm32f4xx_conf.h"
  #define STM32F4XX

#elif (HW_TYPE == HW_TYPE__ROBODYN_F303CC)
  #include "config_robotdyn_f303cc.h"
  #include "stm32f30x.h"
  #define STM32F30X
#else
  #error Не обработан тип платы
#endif

#include "utils/hal_config.h"

#if (FEATURE_ENABLE_USB)
  #include "usb_config.h"
#endif

// ======================================= Declaration =============================================

#endif /* CONFIG_H_ */

/** @} */
