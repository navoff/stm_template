/**
 * @file leds.c
 * @author Vladimir Postnov
 * @date 2 мар. 2019 г.
 * @brief
 * @addtogroup
 * @{
*/

#include "config.h"

#include "drivers/gpio.h"
#include "drivers/tick_timer.h"

#include "leds.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

uint32_t leds_timestamp;

// ====================================== Implementation ===========================================

void leds_init(void)
{
  gpio_rcc_cmd(LED_PORT_IDX, ENABLE);
  gpio_pin_set_mode(LED_PORT_IDX, LED_PIN_IDX, GPIO_MODE_OUT_PP);
  gpio_pin_cmd(LED_PORT_IDX, LED_PIN_IDX, DISABLE);

  START_TIMER(leds_timestamp);
}

void leds_process(void)
{
  if (CHECK_TIMER(leds_timestamp, 500))
  {
    START_TIMER(leds_timestamp);
    
    gpio_pin_toggle(LED_PORT_IDX, LED_PIN_IDX);
  }
}

/** @} */
