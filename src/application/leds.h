/**
 * @file leds.h
 * @author Vladimir Postnov
 * @date 2 мар. 2019 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef LEDS_H_
#define LEDS_H_

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void leds_init(void);
void leds_process(void);

#endif /* LEDS_H_ */

/** @} */
