/**
 * @file main.c
 * @brief
 * @author Vladimir Postnov
 * @date 3 дек. 2016 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include "leds.h"
#include "nvram/nvram_settings_template.h"
#include "drivers/tick_timer.h"
#if (FEATURE_ENABLE_USB)
  #include "app_usb.h"
#endif

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void tick_process(void);

// ====================================== Implementation ===========================================

int main(void)
{
  SystemCoreClockUpdate();
  tick_timer_init(tick_process);
  leds_init();
  nvram_settings_init();
  #if (FEATURE_ENABLE_USB)
  app_usb_init();
  #endif

  uint32_t timestamp_1ms;
  START_TIMER(timestamp_1ms);

  while(1)
  {
    if (CHECK_TIMER(timestamp_1ms, 1))
    {
      START_TIMER(timestamp_1ms);
      
      leds_process();
      nvram_settings_process();
    }
  }
}

void tick_process(void)
{
}

/** @} */
