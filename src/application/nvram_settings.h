/**
 * @file nvram_settings.h
 * @author Vladimir Postnov
 * @date 3 окт. 2017 г.
 * @brief Файл содержит структуру с пользовательскими настройками.
 *        Данная структруа зависит от конкретного проекта.
 * @note Файл необходимо переименовать (новое название "nvram_settings.h") и
 *       поместить в раздел "application" проекта.
 * @addtogroup
 * @{
*/

#ifndef NVRAM_SETTINGS_H_
#define NVRAM_SETTINGS_H_

#include "utils/errors.h"

// ========================================= Definition ============================================

/// Версия настроек NVRAM
#define NVRAM_SETTINGS_VERSION          1

#pragma pack(push, 1)

/// Структура с настройками устройства
/// Добавлять новые поля необходимо в конец
/// При добавлении необходимо увеличить версию настроек NVRAM_SETTINGS_VERSION
typedef struct
{
  uint8_t example_field_1;
  struct
  {
      uint16_t example_field_2;
      uint16_t example_field_3;
  } example_struct_2;
} nvram_settings_t;

#pragma pack(pop)

// ========================================= Declaration ===========================================

void nvram_settings_init(void);
void nvram_settings_process(void);
void nvram_settings_set_default(void);

extern const nvram_settings_t nvram_settings_default;

#endif /* NVRAM_SETTINGS_H_ */

/** @} */
