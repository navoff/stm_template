/**
 * @file nvram_settings.c
 * @author Vladimir Postnov
 * @date 3 окт. 2017 г.
 * @brief
 * @note Файл необходимо переименовать (новое название "nvram_settings.c") и
 *       поместить в раздел "application" проекта.
 * @addtogroup
 * @{
*/

#include "config.h"

#include <stdbool.h>

#include "utils/check_params.h"
#include "nvram/nvram.h"

#include "nvram_settings.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

const nvram_settings_t nvram_settings_default =
{
    .example_field_1 = 0,
    .example_struct_2.example_field_2 = 1,
    .example_struct_2.example_field_3 = 2,
};

nvram_t nvram_settings;

// ======================================== Implementation =========================================

void nvram_settings_init(void)
{
  check_params(sizeof(nvram_settings_t) <= NVRAM_USER_SIZE);

  nvram_init_struct_t init_struct = { 0 };
  if (nvram_struct_init(&init_struct, NVRAM_DRIVER__INT_FLASH) != ERR_OK)
  {
    check_params(false);
  }

  // Инициализация области под хранение настроек
  #if (FEATURE_ENABLE_INT_NVRAM)
  nvram_struct_init(&init_struct, NVRAM_DRIVER__INT_FLASH);
  #elif (FEATURE_ENABLE_EXT_NOR_XX25_NVRAM)
    #error Пока не реализовано
  #else
    #error Не выбран тип flash
  #endif

  init_struct.version = NVRAM_SETTINGS_VERSION;
  if (nvram_init(&nvram_settings, &init_struct) != ERR_OK)
  {
    nvram_settings_set_default();
  }
}

void nvram_settings_process(void)
{
  nvram_process(&nvram_settings);
}

void nvram_settings_set_default(void)
{
  nvram_write_default_struct(
      &nvram_settings, (uint8_t *) &nvram_settings_default, sizeof(nvram_settings_t));
}

/** @} */
