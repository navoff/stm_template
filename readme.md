# Для чего это
...
# Как этим пользоваться
...
## Отладочные платы
### STM32F3DISCOVERY
На сайте [st.com](https://www.st.com/en/evaluation-tools/stm32f3discovery.html)
### NUCLEO-F103RB
На сайте [st.com](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-nucleo-boards/nucleo-f103rb.html)
### NUCLEO-F411RE
На сайте [st.com](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-nucleo-boards/nucleo-f411re.html)
